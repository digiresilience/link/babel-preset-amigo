# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.3](https://gitlab.com/digiresilience.org/link/babel-preset-amigo/compare/0.2.2...0.2.3) (2021-10-08)

### [0.2.2](https://gitlab.com/digiresilience.org/link/babel-preset-amigo/compare/0.2.1...0.2.2) (2021-05-25)

### [0.2.1](https://gitlab.com/digiresilience.org/link/babel-preset-amigo/compare/0.2.0...0.2.1) (2021-05-03)


### Features

* bump babel to 7.14 ([fec59d5](https://gitlab.com/digiresilience.org/link/babel-preset-amigo/commit/fec59d563dc0b0f1c3ace754d88091f0bdbf1afc))

## [0.2.0](https://gitlab.com/digiresilience.org/link/babel-preset-amigo/compare/0.1.0...0.2.0) (2020-11-20)


### ⚠ BREAKING CHANGES

* upgrade deps

### Features

* upgrade deps ([46a9ff0](https://gitlab.com/digiresilience.org/link/babel-preset-amigo/commit/46a9ff0883e1f99ab0e918fcbe8c90f4545d58cf))

## 0.1.0 (2020-10-09)
